﻿using Microsoft.Extensions.Configuration;
using Microsoft.Extensions.Options;
using Microsoft.IdentityModel.Tokens;
using ScheduleAuthentication.Entities;
using ScheduleAuthentication.Helper;
using System;
using System.Collections.Generic;
using System.Data;
using System.Data.SqlClient;
using System.IdentityModel.Tokens.Jwt;
using System.Linq;
using System.Security.Claims;
using System.Text;
using System.Threading.Tasks;

namespace ScheduleAuthentication.Services
{
    public class UserService : IUserService
    {
        private readonly string _connectionString;
        // users hardcoded for simplicity, store in a db with hashed passwords in production applications
        private List<User> _users = new List<User>
        {
            new User { Id = 1, FirstName = "Test", LastName = "User", Username = "test", Password = "test" }
        };
        private SqlConnection _sqlConnection;
        private SqlCommand _sqlCommand;

        private readonly AppSettings _appSettings;

        public UserService(IOptions<AppSettings> appSettings, IConfiguration configuration)
        {
            _appSettings = appSettings.Value;
            _connectionString = configuration.GetConnectionString("defaultConnection");
            _sqlConnection = new SqlConnection(_connectionString);
            _sqlCommand = _sqlConnection.CreateCommand();
            _sqlCommand.CommandType = CommandType.StoredProcedure;
        }

        public User ValidateUser(string username, string password)
        {
            _sqlConnection.Open();
            _sqlCommand.CommandText = "[dbo].[Proc_ValidateUser]";
            _sqlCommand.Parameters.AddWithValue("@username", username);
            _sqlCommand.Parameters.AddWithValue("@password", password);
            SqlDataReader sqlDataReader = _sqlCommand.ExecuteReader();
            var response = new User();
            while (sqlDataReader.Read())
            {
                response = MapToValue(sqlDataReader);
            }
            _sqlConnection.Close();
            return response;
        }

        public User Authenticate(string username, string password)
        {
            User user = ValidateUser(username,password);
           
            if (user == null)
                return null;

            // authentication successful so generate jwt token
            var tokenHandler = new JwtSecurityTokenHandler();
            var key = Encoding.ASCII.GetBytes(_appSettings.Secret);
            var tokenDescriptor = new SecurityTokenDescriptor
            {
                Subject = new ClaimsIdentity(new Claim[]
                {
                    new Claim("FullName", user.Username.ToString()),
                    new Claim("PermissionID", user.PermissionId.ToString())
                }),
                Expires = DateTime.UtcNow.AddDays(30),
                SigningCredentials = new SigningCredentials(new SymmetricSecurityKey(key), SecurityAlgorithms.HmacSha256Signature)
            };
            var token = tokenHandler.CreateToken(tokenDescriptor);
            user.Token = tokenHandler.WriteToken(token);

            return user;
        }

        private User MapToValue(SqlDataReader reader)
        {
            return new User()
            {
                // Id = (int)reader["Id"],
                Username = reader["username"].ToString(),
                Usercode = reader["usercode"].ToString(),
                PermissionId = (int)reader["perid"]
            };
        }

        public IEnumerable<User> GetAll()
        {
            return _users;
        }
    }
}
