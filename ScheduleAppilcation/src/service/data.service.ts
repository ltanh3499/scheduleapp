import { Injectable } from '@angular/core';
import { BehaviorSubject, Subject } from 'rxjs';

@Injectable({
  providedIn: 'root'
})
export class DataService {

  typeWork = new Subject<any>();
  constructor() { }

  ChooseTypeOfWork(value) {
    this.typeWork.next(value);
  }
}
