import { Injectable } from '@angular/core';
import { Observable, throwError } from 'rxjs';
import { catchError, retry } from 'rxjs/operators';
import { HttpClient, HttpHeaders, HttpParams } from '@angular/common/http';


@Injectable({
  providedIn: 'root'
})
export class ScheduleService {

  constructor(private http: HttpClient) { }

  getUser(){
    return this.http.get("https://localhost:44307/api/user");
  }

  getListSchedule(userid: number) {
    return this.http.get("https://localhost:44307/api/schedule/"+userid);
  }

  getListScheduleByCode(usercode: Array<string>) {
    return this.http.post("https://localhost:44307/api/schedule", usercode);
    // return this.http.get("https://localhost:44307/api/schedule");
  }

  saveWorkEachDay(param) {
    return this.http.post("https://localhost:44307/api/savework",param);
  }

  addnewEmployee(param) {
    return this.http.post("https://localhost:44307/api/user",param)
  }

  addSchedule(param){
    return this.http.post("https://localhost:44307/api/addschedule",param);
  }
}
