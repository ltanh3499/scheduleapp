import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import { ScheduleComponent } from './schedule.component';
import { PopupAddEmployeeComponent } from './popup-add-employee/popup-add-employee.component';
import { BrowserModule } from '@angular/platform-browser';
import { AppRoutingModule } from '../app-routing.module';
import { FormsModule } from '@angular/forms';
import { FullCalendarModule } from '@fullcalendar/angular';
import { MatDialogModule } from '@angular/material/dialog';
import { BrowserAnimationsModule } from '@angular/platform-browser/animations';
import { PopupAddScheduleComponent } from './popup-add-schedule/popup-add-schedule.component';
import { PopupChooseTypeDateComponent } from './popup-add-schedule/popup-choose-type-date/popup-choose-type-date.component';
import { ListEmployeeComponent } from './list-employee/list-employee.component';
import { EmployeeComponent } from './list-employee/employee/employee.component';



@NgModule({
  declarations: [
    ScheduleComponent,
    PopupAddEmployeeComponent,
    PopupAddScheduleComponent,
    PopupChooseTypeDateComponent,
    ListEmployeeComponent,
    EmployeeComponent
  ],
  imports: [
    CommonModule,
    AppRoutingModule,
    FormsModule,
    FullCalendarModule, // for FullCalendar!
    MatDialogModule,
  ],
  exports: [
    ScheduleComponent
  ],
  entryComponents: [
    PopupAddEmployeeComponent
  ],
})
export class ScheduleModule { }
