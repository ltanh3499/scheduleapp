import { Component, OnInit, ViewChild } from '@angular/core';
import dayGridPlugin from '@fullcalendar/daygrid';
import { MatDialog, MatDialogConfig } from '@angular/material/dialog';
import { PopupAddEmployeeComponent } from './popup-add-employee/popup-add-employee.component';
import { PopupAddScheduleComponent } from './popup-add-schedule/popup-add-schedule.component';
import { Calendar, EventInput } from '@fullcalendar/core';
import timeGridPlugin from '@fullcalendar/timegrid';
import interactionPlugin, { ThirdPartyDraggable } from '@fullcalendar/interaction';
import listPlugin from '@fullcalendar/list';
import { FullCalendarComponent } from '@fullcalendar/angular';
import { ListEmployeeComponent } from './list-employee/list-employee.component';
import { ScheduleService } from 'src/service/schedule.service';


@Component({
  selector: 'app-schedule',
  templateUrl: './schedule.component.html',
  styleUrls: ['./schedule.component.scss']
})
export class ScheduleComponent implements OnInit {

  @ViewChild('calendar') calendarComponent: FullCalendarComponent; // the #calendar in the template
  constructor(public dialog: MatDialog,
    private scheduleSV: ScheduleService
    ) { }

  ngOnInit(): void {
    this.getListScheduleByUserid();
  }

  calendarVisible = true;
  calendarPlugins = [dayGridPlugin, timeGridPlugin, interactionPlugin];
  calendarWeekends = true;
  calendarEvents: EventInput[] = [
    //{ title: 'Event Now', start: new Date() },
  ];

  toggleVisible() {
    this.calendarVisible = !this.calendarVisible;
  }

  toggleWeekends() {
    this.calendarWeekends = !this.calendarWeekends;
  }

  gotoPast() {
    let calendarApi = this.calendarComponent.getApi();
    calendarApi.gotoDate('2000-01-01'); // call a method on the Calendar object
  }

  handleDateClick(arg) {
    let listEmpOfDay: Array<string> = [];
    this.calendarEvents.forEach(e => {
      if(e.start.toString().substring(0,10) === arg.date.toString().substring(0,10)) {
        listEmpOfDay.push(e.title);
      }
    });
    this.dialog.open(ListEmployeeComponent,{
      height: '400px',
      width: '300px',
      data: {
        listEmpOfDay: listEmpOfDay
      },
      disableClose: true,
      hasBackdrop: true
    }).afterClosed()
    .subscribe(data => {
      this.getListScheduleByUserid();
    })
      // this.calendarEvents = this.calendarEvents.concat({ // add new event data. must create new array
      //   title: 'New Event',
      //   start: arg.date,
      //   allDay: arg.allDay
      // })
  }

  getListScheduleByUserid(){
    const userid = 2;
    this.scheduleSV.getListSchedule(userid)
    .subscribe((data: Array<Object>) => {
      this.calendarEvents = [];
      data.forEach(e => {
        // let calendarObj = {title: e["usercode"], start: new Date(e["date"])}
        // this.calendarEvents.push(calendarObj);
        this.calendarEvents = this.calendarEvents.concat({ // add new event data. must create new array
          title: e["usercode"],
          start: new Date(e["date"]),
          allDay: true
        })
      })
    });
  }
  

  openDialogAddEmp() {
    const dialogRèf = this.dialog.open(PopupAddEmployeeComponent,{
      height: '600px',
      width: '400px',
      data: {
      },
      disableClose: true,
      hasBackdrop: true
    });
  }

  openDialogAddSchedule() {
    const dialogRèf = this.dialog.open(PopupAddScheduleComponent, {
      height: '600px',
      width: '1200px',
      data: {
      },
      disableClose: true,
      hasBackdrop: true
    });

    dialogRèf.afterClosed()
    .subscribe(data =>{
      this.getListScheduleByUserid();
    })
  }

}
