import { Component, OnInit, Inject } from '@angular/core';
import { MatDialogRef, MatDialog, MAT_DIALOG_DATA } from '@angular/material/dialog';
import { TypeWork } from 'src/enum/type-work.enum';
import { ScheduleService } from 'src/service/schedule.service';

@Component({
  selector: 'app-employee',
  templateUrl: './employee.component.html',
  styleUrls: ['./employee.component.scss']
})
export class EmployeeComponent implements OnInit {

  isEdit: boolean = false;
  username: string;
  workType: string;
  workTypeID: number;
  workDescription: string;
  note: string; 
  date: any;
  scheduleID: number;
  constructor(public dialogRef: MatDialogRef<EmployeeComponent>,
    @Inject(MAT_DIALOG_DATA)public data: any,
    public dialog: MatDialog,
    private scheduleSV: ScheduleService
    ) { }

  ngOnInit(): void {
    if(this.data && this.data.dataDialog){
      this.username = this.data.dataDialog["username"];
      this.workTypeID = this.data.dataDialog["typeid"];
      switch(this.workTypeID) {
        case TypeWork.day: {
          this.workType = "Cả ngày";
          break;
        }
        case TypeWork.afternoon: {
          this.workType = "Buổi chiều";
          break;
        }
        case TypeWork.morning: {
          this.workType = "Buổi sáng";
          break;
        }
      }
      this.workDescription = this.data.dataDialog["worknote"];
      this.date = new Date(this.data.dataDialog["date"]);
      this.scheduleID = this.data.dataDialog["scheduleid"];
    }
  }

  closeDialog() {
    this.dialogRef.close();
  }

  turnEditMode() {
    this.isEdit = true;
  }

  saveEdit() {
    const me = this;
    let objReq = {
      username: this.username,
      typeid: this.workTypeID,
      worknote: this.workDescription,
      scheduleid: this.scheduleID
    };

    this.scheduleSV.saveWorkEachDay(objReq)
      .subscribe(data =>{
        this.isEdit = false;
        switch(this.workTypeID) {
          case TypeWork.day: {
            this.workType = "Cả ngày";
            break;
          }
          case TypeWork.afternoon: {
            this.workType = "Buổi chiều";
            break;
          }
          case TypeWork.morning: {
            this.workType = "Buổi sáng";
            break;
          }
        }
      });
    this.isEdit = false;
  }

  exitEditMode(){
    this.isEdit = false;
  }
}
