import { Component, OnInit, Inject } from '@angular/core';
import { MatDialogRef, MAT_DIALOG_DATA, MatDialog } from '@angular/material/dialog';
import { EmployeeComponent } from './employee/employee.component';
import { ScheduleService } from 'src/service/schedule.service';
import { element } from 'protractor';
import { User } from 'src/Model/user';
import { ITS_JUST_ANGULAR } from '@angular/core/src/r3_symbols';

@Component({
  selector: 'app-list-employee',
  templateUrl: './list-employee.component.html',
  styleUrls: ['./list-employee.component.scss']
})
export class ListEmployeeComponent implements OnInit {

  listEmployee: Array<string> = [];
  listEmployeeCode: Array<string> = [];
  listEmployeeFullData = [];
  constructor(public dialogRef: MatDialogRef<ListEmployeeComponent>,
    @Inject(MAT_DIALOG_DATA)public data: any,
    public dialog: MatDialog,
    private scheduleSV: ScheduleService) { }

  ngOnInit(): void {
    if(this.data && this.data.listEmpOfDay) {
      this.data.listEmpOfDay.forEach(e => {
        this.listEmployeeCode.push(e)
      })
      this.getListEmployeeByCode();
      
    }
  }

  clickEmployee(username){

    let dataDialog;
    this.listEmployeeFullData.forEach(e => {
      if(e["username"] === username) {
        dataDialog = e;
      }
    })
    this.dialog.open(EmployeeComponent,{
      height: "600px",
      width: "400px",
      data: {
        dataDialog: dataDialog
      }
    })
    this.closeDialog();
  }

  getListEmployee(){
    this.scheduleSV.getUser()
    .subscribe((data: Array<User>) => {
      if(data) {
        data.forEach(element => {
          this.listEmployee.push(element.username);
        });
      }
    })
  }

  getListEmployeeByCode() {
    let usercodeObj = []
    this.listEmployeeCode.forEach(e => {
      usercodeObj.push({usercode: e});
    });

    this.scheduleSV.getListScheduleByCode(usercodeObj)
    .subscribe((data: Array<User>) =>{

      for (let i = 0; i < data.length; i++) {
        this.listEmployee.push(data[i][0]["username"]);
        this.listEmployeeFullData.push(data[i][0]);
      }
    })
  }

  bindingListEmployee(){
    const $li = "" 
  }
  closeDialog() {
    this.dialogRef.close();
  }

}
