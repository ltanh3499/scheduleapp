import { Component, OnInit, Inject } from '@angular/core';
import { MAT_DIALOG_DATA, MatDialogRef } from '@angular/material/dialog';
import { ScheduleService } from 'src/service/schedule.service';

@Component({
  templateUrl: './popup-add-employee.component.html',
  styleUrls: ['./popup-add-employee.component.scss']
})
export class PopupAddEmployeeComponent implements OnInit {

  username: string;
  password: string;
  constructor(
    public dialogRef: MatDialogRef<PopupAddEmployeeComponent>,
    @Inject(MAT_DIALOG_DATA) public data: any,
    private scheduleSV: ScheduleService)
  { }

  ngOnInit(): void {
    
  }

  addEmployee(){
    if (this.username && this.password) {
      const obj = {"username": this.username, "password": this.password};
      this.scheduleSV.addnewEmployee(obj)
        .subscribe(data => {
          if(data > 0) {
            console.log(data);
          }
        })
      }
      this.dialogRef.close()
  }

  onNoClick() {
    this.dialogRef.close()
  }
}
