import { Component, OnInit, Inject, ViewChild } from '@angular/core';
import dayGridPlugin from '@fullcalendar/daygrid';
import { MatDialogRef, MAT_DIALOG_DATA, MatDialog } from '@angular/material/dialog';
import { FullCalendarComponent } from '@fullcalendar/angular';
import { EventInput } from '@fullcalendar/core';
import timeGridPlugin from '@fullcalendar/timegrid';
import interactionPlugin from '@fullcalendar/interaction';
import { PopupChooseTypeDateComponent } from './popup-choose-type-date/popup-choose-type-date.component';
import { DataService } from 'src/service/data.service';
import { ScheduleService } from 'src/service/schedule.service';

@Component({
  selector: 'app-popup-add-schedule',
  templateUrl: './popup-add-schedule.component.html',
  styleUrls: ['./popup-add-schedule.component.scss']
})
export class PopupAddScheduleComponent implements OnInit {

  // calendarPlugins = [dayGridPlugin]; // important!
  date: any;
  @ViewChild('calendar') calendarComponent: FullCalendarComponent; // the #calendar in the template
  constructor(public dialogRef: MatDialogRef<PopupAddScheduleComponent>,
    @Inject(MAT_DIALOG_DATA) public data: any,
    public dialog: MatDialog,
    private dataSV: DataService,
    private scheduleSV: ScheduleService
  ) { }

  ngOnInit(): void {
    this.getListScheduleByUserid();

  }

  calendarVisible = true;
  calendarPlugins = [dayGridPlugin, timeGridPlugin, interactionPlugin];
  calendarWeekends = true;
  calendarEvents: EventInput[] = [
  ];

  toggleVisible() {
    this.calendarVisible = !this.calendarVisible;
  }

  toggleWeekends() {
    this.calendarWeekends = !this.calendarWeekends;
  }

  gotoPast() {
    let calendarApi = this.calendarComponent.getApi();
    calendarApi.gotoDate('2000-01-01'); // call a method on the Calendar object
  }

  handleDateClick(arg) {
    this.date = arg.date;
    const dl = this.dialog.open(PopupChooseTypeDateComponent, {
      height: '300px',
      width: '400px',
      data: {
      },
      disableClose: true,
      hasBackdrop: true
    });
    dl.afterClosed()
      .subscribe(data => {
        if (data) {
          let dataObj = {
            "userid": "2",
            "typeid": data,
            "date": this.date
          };

          this.scheduleSV.addSchedule(dataObj)
            .subscribe(data => {
              this.calendarEvents = this.calendarEvents.concat({ // add new event data. must create new array
                title: 'ltanh1',
                start: this.date
              });
            })
        }
      });

  }


  getListScheduleByUserid() {
    const userid = 2;
    this.scheduleSV.getListSchedule(userid)
    .subscribe((data: Array<Object>) => {
      this.calendarEvents = [];
        data.forEach(e => {
          // let calendarObj = {title: e["usercode"], start: new Date(e["date"])}
          this.calendarEvents = this.calendarEvents.concat({ // add new event data. must create new array
            title: e["usercode"],
            start: new Date(e["date"]),
            allDay: true
          })
        })
      });
  }


  closeDialog() {
    this.dialogRef.close();

  }
}
