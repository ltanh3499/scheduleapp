import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { PopupChooseTypeDateComponent } from './popup-choose-type-date.component';

describe('PopupChooseTypeDateComponent', () => {
  let component: PopupChooseTypeDateComponent;
  let fixture: ComponentFixture<PopupChooseTypeDateComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ PopupChooseTypeDateComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(PopupChooseTypeDateComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
