import { Component, OnInit, Inject, Output, EventEmitter } from '@angular/core';
import { MatDialogRef, MAT_DIALOG_DATA } from '@angular/material/dialog';
import { DataService } from 'src/service/data.service';

@Component({
  selector: 'app-popup-choose-type-date',
  templateUrl: './popup-choose-type-date.component.html',
  styleUrls: ['./popup-choose-type-date.component.scss']
})
export class PopupChooseTypeDateComponent implements OnInit {

  workType: number = 3;
  constructor(public dialogRef: MatDialogRef<PopupChooseTypeDateComponent>,
    @Inject(MAT_DIALOG_DATA) public data: any,
    private dataSV: DataService
    ) { }

  ngOnInit(): void {
  }

  submitTypeOfWork(){
    const me = this;
    if(this.workType) {
      me.dataSV.ChooseTypeOfWork(me.workType);
      this.dialogRef.close(me.workType);
    }
  }

  DeleteSchedule() {
    
  }
  
  closeDialog() {
    this.dialogRef.close();
  }
}
