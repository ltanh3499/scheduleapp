import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { PopupAddScheduleComponent } from './popup-add-schedule.component';

describe('PopupAddScheduleComponent', () => {
  let component: PopupAddScheduleComponent;
  let fixture: ComponentFixture<PopupAddScheduleComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ PopupAddScheduleComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(PopupAddScheduleComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
