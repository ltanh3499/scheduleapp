﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Schedule.BO
{
    public class Account
    {
        public string usercode { get; set; }
        public string password { get; set; }
        public int userid { get; set; }
        public string username { get; set; }
    }
}
