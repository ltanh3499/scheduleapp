﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Schedule.BO
{
    public class ScheduleRequest
    {
        public int userid { get; set; }
        public int typeid { get; set; }
        public DateTime date { get; set; }
    }
}
