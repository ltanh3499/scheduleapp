﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Schedule.BO
{
    public class WorkItem
    {
        public int typeid { get; set; }
        public string username { get; set; }
        public string worknote { get; set; }
        public int scheduleid { get; set; }
    }
}
