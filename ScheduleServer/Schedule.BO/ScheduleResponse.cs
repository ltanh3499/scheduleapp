﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Schedule.BO
{
    public class ScheduleResponse
    {
        public int scheduleid { get; set; }
        public DateTime date { get; set; }
        public string worknote { get; set; }
        public string username { get; set; }
        public string usercode { get; set; }
        public int typeid { get; set; }
        public int perid { get; set; }
    }
}
