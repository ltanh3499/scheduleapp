﻿using Schedule.BO;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Schedule.BL.BLSchedule
{
    public interface IBLSchedule
    {
        IEnumerable<ScheduleResponse> GetListSchedule(int userid);
        int AddSchedule(ScheduleRequest schedule);
    }
}
