﻿using Schedule.BO;
using Schedule.DL.DLSchedule;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Schedule.BL.BLSchedule
{
    public class BLSchedule: IBLSchedule
    {
        IDLSchedule _IDLSchedule;
        public BLSchedule()
        {
            _IDLSchedule = new DLSchedule();
        }

        public IEnumerable<ScheduleResponse> GetListSchedule(int userid)
        {
            return _IDLSchedule.GetListSchedule(userid);
        }

        public int AddSchedule(ScheduleRequest schedule)
        {
            return _IDLSchedule.AddSchedule(schedule);
        }

    }
}
