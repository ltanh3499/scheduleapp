﻿using Schedule.BO;
using Schedule.DL;
using System;
using System.Collections.Generic;
using System.Diagnostics;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Schedule.BL
{
    public class BLUser: IBLUser
    {
        IDLUser _IDLUser;
        public BLUser()
        {
            _IDLUser = new DLUser();
        }

        public IEnumerable<Account> GetUsers()
        {
            return _IDLUser.GetUser();
        }
        public int SaveWork(string username, int typeid, string worknote, int scheduleid)
        {
            return _IDLUser.SaveWork(username,typeid,worknote, scheduleid);
        }

        public List<IEnumerable<ScheduleResponse>> GetListUserByCode(Usercode[] usercode)
        {
            List<IEnumerable<ScheduleResponse>> ListSchedule = new List<IEnumerable<ScheduleResponse>>();
            foreach (Usercode item in usercode)
            {
                ListSchedule.Add(_IDLUser.GetListUserByCode(item));
            }
            return ListSchedule;
        }

        public int AddUser(Account account)
        {
            string phrase = account.username;
            string[] words = phrase.Split(' ');
            string userCode = String.Empty;
            for (int i = 0; i < words.Length; i++)
            {
                if (i == 0)
                {
                    userCode += words[i].Substring(0, 1);
                }
                if (i == words.Length - 2)
                {
                    userCode += words[i].Substring(0, 1);
                }
                if (i == words.Length - 1)
                {
                    userCode += words[i];
                }
            }

            account.usercode = userCode.ToLower();

            return _IDLUser.AddUser(account);
        }
    }
}
