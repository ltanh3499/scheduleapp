﻿using Schedule.BO;
using Schedule.DL;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Schedule.BL
{
    public interface IBLUser
    {
        IEnumerable<Account> GetUsers();

        List<IEnumerable<ScheduleResponse>> GetListUserByCode(Usercode[] usercode);

        int SaveWork(string username, int typeid, string worknote, int date);

        int AddUser(Account account);
    }
}
