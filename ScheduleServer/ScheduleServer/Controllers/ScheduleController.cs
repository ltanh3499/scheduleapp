﻿using Schedule.BL;
using Schedule.BL.BLSchedule;
using Schedule.BO;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Net;
using System.Net.Http;
using System.Web.Http;
using System.Web.Http.Cors;

namespace ScheduleServer.Controllers
{
    [RoutePrefix("api")]
    public class ScheduleController : ApiController
    {
        IBLUser _IBLUser;
        IBLSchedule _IBLSchedule;
        public ScheduleController()
        {
            _IBLUser = new BLUser();
            _IBLSchedule = new BLSchedule();
        }

        [HttpGet]
        [Route("user")]
        public IEnumerable<Account> GetUsers()
        {
            return _IBLUser.GetUsers();
        }

        [HttpGet]
        [Route("schedule/{userid}")]
        public IEnumerable<ScheduleResponse> GetListSchedule([FromUri]int userid)
        {
            return _IBLSchedule.GetListSchedule(userid);
        }

        [HttpPost]
        [Route("schedule")]
        public List<IEnumerable<ScheduleResponse>> GetListScheduleByUsercode([FromBody]Usercode[] usercode)
        {
            return _IBLUser.GetListUserByCode(usercode);
        }

        [HttpPost]
        [Route("savework")]
        public int SaveWork(WorkItem workItem)
        {
            return _IBLUser.SaveWork(workItem.username, workItem.typeid, workItem.worknote,workItem.scheduleid);
        }

        [HttpPost]
        [Route("user")]
        public int AddEmployee(Account account)
        {
            return _IBLUser.AddUser(account);
        }

        [HttpPost]
        [Route("addschedule")]
        public int AddSchedule(ScheduleRequest schedule)
        {
            schedule.date = schedule.date.AddDays(1);
            return _IBLSchedule.AddSchedule(schedule);
        }
    }
}
