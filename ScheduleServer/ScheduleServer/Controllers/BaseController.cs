﻿using Schedule.BL;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Net;
using System.Net.Http;
using System.Web.Http;

namespace ScheduleServer.Controllers
{
    public class BaseController : ApiController
    {
        public IBLUser _IBLUser;
        public BaseController()
        {
            _IBLUser = new BLUser();
        }
    }
}
