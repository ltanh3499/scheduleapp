﻿using Schedule.BO;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Schedule.DL.DLSchedule
{
    public interface IDLSchedule
    {
        IEnumerable<ScheduleResponse> GetListSchedule(int userid);
        int AddSchedule(ScheduleRequest schedule);
    }
}
