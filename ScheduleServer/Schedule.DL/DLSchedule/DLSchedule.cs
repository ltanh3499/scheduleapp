﻿using Schedule.BO;
using System;
using System.Collections.Generic;
using System.Data;
using System.Data.SqlClient;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Schedule.DL.DLSchedule
{
    public class DLSchedule: IDLSchedule
    {
        private readonly string _connectString = "Data Source= DESKTOP-A5GT3RF\\SQLEXPRESS; Initial Catalog=Schedule;Integrated Security = true";
        private SqlConnection _sqlConnection;
        private SqlCommand _sqlCommand;
        public DLSchedule()
        {
            _sqlConnection = new SqlConnection(_connectString);
            _sqlCommand = _sqlConnection.CreateCommand();
            _sqlCommand.CommandType = CommandType.StoredProcedure;
        }

        public IEnumerable<ScheduleResponse> GetListSchedule(int userid)
        {
            _sqlConnection.Open();
            _sqlCommand.CommandText = "[dbo].[Proc_GetListSchedule]";
            _sqlCommand.Parameters.AddWithValue("@userid", userid);
            SqlDataReader sqlDataReader = _sqlCommand.ExecuteReader();
            while (sqlDataReader.Read())
            {
                var schedule = new ScheduleResponse();
                for (var i = 0; i < sqlDataReader.FieldCount; i++)
                {
                    var FieldName = sqlDataReader.GetName(i);
                    var FieldValue = sqlDataReader.GetValue(i);
                    var property = schedule.GetType().GetProperty(FieldName);
                    if (property != null && FieldValue != DBNull.Value)
                    {
                        property.SetValue(schedule, FieldValue);
                    }
                }
                yield return schedule;
            }
            _sqlConnection.Close();
        }

        public int AddSchedule(ScheduleRequest schedule)
        {
            _sqlConnection.Open();
            _sqlCommand.CommandText = "[dbo].[Proc_AddSchedule]";
            _sqlCommand.Parameters.AddWithValue("@userid", schedule.userid);
            _sqlCommand.Parameters.AddWithValue("@date", schedule.date);
            _sqlCommand.Parameters.AddWithValue("@typeid", schedule.typeid);
            var result = _sqlCommand.ExecuteNonQuery();
            _sqlConnection.Close();
            return result;
        }
    }
}
