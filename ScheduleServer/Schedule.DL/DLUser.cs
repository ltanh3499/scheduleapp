﻿using Schedule.BO;
using System;
using System.Collections.Generic;
using System.Data;
using System.Data.SqlClient;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Schedule.DL
{
    public class DLUser : IDLUser
    {
        private readonly string _connectString = "Data Source= DESKTOP-A5GT3RF\\SQLEXPRESS; Initial Catalog=Schedule;Integrated Security = true";
        private SqlConnection _sqlConnection;
        private SqlCommand _sqlCommand;

        public DLUser()
        {
            _sqlConnection = new SqlConnection(_connectString);
            _sqlCommand = _sqlConnection.CreateCommand();
            _sqlCommand.CommandType = CommandType.StoredProcedure;
        }


        public IEnumerable<Account> GetUser()
        {
            _sqlConnection.Open();
            _sqlCommand.CommandText = "[dbo].[Proc_GetListAccount]";

            SqlDataReader sqlDataReader = _sqlCommand.ExecuteReader();
            while (sqlDataReader.Read())
            {
                var account = new Account();
                for (var i = 0; i < sqlDataReader.FieldCount; i++)
                {
                    var FieldName = sqlDataReader.GetName(i);
                    var FieldValue = sqlDataReader.GetValue(i);
                    var property = account.GetType().GetProperty(FieldName);
                    if (property != null && FieldValue != DBNull.Value)
                    {
                        property.SetValue(account, FieldValue);
                    }
                }
                yield return account;
            }
            _sqlConnection.Close();
        }

        public int SaveWork(string username, int typeid, string worknote, int date)
        {
            _sqlConnection.Open();
            _sqlCommand.CommandText = "[dbo].[Proc_SaveWork]";
            _sqlCommand.Parameters.AddWithValue("@username", username);
            _sqlCommand.Parameters.AddWithValue("@typeid", typeid);
            _sqlCommand.Parameters.AddWithValue("@worknote", worknote);
            _sqlCommand.Parameters.AddWithValue("@scheduleid", date);
            var result = _sqlCommand.ExecuteNonQuery();
            _sqlConnection.Close();
            return result;
        }

        public IEnumerable<ScheduleResponse> GetListUserByCode(Usercode employeeCode)
        {
            _sqlConnection.Open();
            _sqlCommand.CommandText = "[dbo].[Proc_GetListEmployeeByCode]";
            _sqlCommand.Parameters.Clear();
            _sqlCommand.Parameters.AddWithValue("@usercode", employeeCode.usercode);

            SqlDataReader sqlDataReader = _sqlCommand.ExecuteReader();
            while (sqlDataReader.Read())
            {
                var account = new ScheduleResponse();
                for (var i = 0; i < sqlDataReader.FieldCount; i++)
                {
                    var FieldName = sqlDataReader.GetName(i);
                    var FieldValue = sqlDataReader.GetValue(i);
                    var property = account.GetType().GetProperty(FieldName);
                    if (property != null && FieldValue != DBNull.Value)
                    {
                        property.SetValue(account, FieldValue);
                    }
                }
                yield return account;
            }
            _sqlConnection.Close();
        }

        public int AddUser(Account account)
        {
            _sqlConnection.Open();
            _sqlCommand.CommandText = "[dbo].[Proc_AddUser]";
            _sqlCommand.Parameters.AddWithValue("@username", account.username);
            _sqlCommand.Parameters.AddWithValue("@password", account.password);
            _sqlCommand.Parameters.AddWithValue("@usercode", account.usercode);
            var result = _sqlCommand.ExecuteNonQuery();
            _sqlConnection.Close();
            return result;
        }
    }
}
