﻿using Schedule.BO;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Schedule.DL
{
    public interface IDLUser
    {
        IEnumerable<Account> GetUser();

        IEnumerable<ScheduleResponse> GetListUserByCode(Usercode employeeCode);

        int SaveWork(string username, int typeid, string worknote, int date);

        int AddUser(Account account);
    }
}
